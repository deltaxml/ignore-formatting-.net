@echo off
..\..\bin\deltaxml-docbook.exe compare  ^
                docbook-article-version-1.xml docbook-article-version-2.xml docbook-article-revision.xml ^
                indent=yes ignore-inline-formatting=true