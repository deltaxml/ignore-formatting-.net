﻿// Copyright (c) 2024 Deltaman group limited. All rights reserved.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

// Add System IO for file handling.
using System.IO;

// Add the main DocBook Compare .Net API
using DeltaXML.DocBookCompareApi;

// Add the underpinning (IKVM cross compiled) Java API
// (This includes some parameter types and exceptions)
using com.deltaxml.docbook;

namespace DocBookDotNetSample
{
    class Program
    {
        /// <summary>
        /// This is the main method of the Program class, which is invoked at the command line.
        /// </summary>
        /// <param name="args">The array of command line arguments.</param>
        public static void Main(string[] args)
        {
            try
            {
                // Get a fresh comparison object
                DocBookCompareDotNet docbookCompare = new DocBookCompareDotNet();

                // Enable the output indentation option/parameter
                docbookCompare.Indent = IndentOutput.YES;
                docbookCompare.IgnoreInlineFormatting = false;

                // Provide variables for each of the file-based compare method arguments
                FileInfo inFile1= new FileInfo("docbook-article-version-1.xml");
                FileInfo inFile2= new FileInfo("docbook-article-version-2.xml");
                FileInfo outFile1= new FileInfo("result-no-format-ignoring.xml");
                FileInfo outFile2= new FileInfo("result-ignoring-formatting.xml");

                // Run the compare (with pre/post commentry)
                Console.WriteLine("Comparing files '"+inFile1+"' and '"+inFile2+"', without ignoring formatting");
                docbookCompare.compare(inFile1, inFile2, outFile1);
                Console.WriteLine("Storing results in file '"+outFile1+"'");
                
                docbookCompare.IgnoreInlineFormatting = true;
                
                Console.WriteLine("Comparing files '"+inFile1+"' and '"+inFile2+"', ignoring format changes");
                docbookCompare.compare(inFile1, inFile2, outFile2);
                Console.WriteLine("Storing results in file '"+outFile2+"'");
            }
            catch (FileNotFoundException e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
            }
            catch (DeltaXMLDocBookException javaException)
            {
                // The DocBook compare method throws IKVM-java exceptions, these need to be
                // cast into .NET exceptions before the Message and StackTrace properties
                // are available.
                Exception e = (Exception)javaException;

                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
            }
            // Ensure Visual Studio debug console window does not vanish on successful completion.
            Console.WriteLine();
            Console.WriteLine("Press return to continue");
            Console.ReadLine();
        }
    }
}
